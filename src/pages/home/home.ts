import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  msg: string = "";
  newName: string = "";
  names: string[] = [];

  constructor(public navCtrl: NavController) {
  }

  onClear() {
    this.names.length = 0;
    this.msg = "Data was cleared."
  }
  onAdd() {
    if(this.newName.length==0) {
      alert("Please type new name");
      return;
    }
    this.names.push(this.newName);
    this.msg = this.newName + " was added."
    this.newName = "";
  }
  onPop() {
    this.msg = this.names.pop() + " was removed from the tail";
  }
  onShift() {
    this.msg = this.names.shift() + " was removed from the head";
  }
  onSort() {
    this.names.sort();
    this.msg = "Data sorted."
  }
}
